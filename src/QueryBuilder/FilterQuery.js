"use strict";

const _ = require("lodash");

class FilterQuery {
  constructor() {
    this._hasChild = [];
    this._hasParent = [];
    this._whereConditions = [];
  }

  /**
   * This method is used to get where conditions
   *
   * @returns {Array}
   */
  get conditions() {
    return this._whereConditions;
  }

  /**
   * This method is used to add multiple where conditions to dsl
   *
   * @param {Array<Object>} conditions
   * @returns {FilterQuery}
   */
  whereAll(conditions) {
    if (!_.isEmpty(conditions)) {
      conditions.forEach((condition) => {
        this.where(condition);
      });
    }
    return this;
  }

  /**
   * This method is used to add where condition to dsl
   *
   * @param {Object} condition
   * @returns {FilterQuery}
   */
  where(condition) {
    this._whereConditions.push(condition);
    return this;
  }

  /**
   * This method is used to add match condition to dsl
   *
   * @param {String} attribute
   * @param {String|Boolean|Number} value
   * @returns {FilterQuery}
   */
  match(attribute, value) {
    return this.where({
      match: {
        [attribute]: value
      }
    });
  }

  /**
   * This method is used to add term condition to dsl
   *
   * @param {String} attribute
   * @param {String|Boolean|Number} value
   * @returns {FilterQuery}
   */
  term(attribute, value) {
    return this.where({
      term: {
        [attribute]: value
      }
    });
  }

  /**
   * This method is used to add terms condition to dsl
   *
   * @param {String} attribute
   * @param {Array<String|Boolean|Number>} values
   * @returns {FilterQuery}
   */
  terms(attribute, values) {
    return this.where({
      terms: {
        [attribute]: values
      }
    });
  }

  /**
   * This method is used to add range (greater than) condition to dsl
   *
   * @param {String} attribute
   * @param {String|Boolean|Number} value
   * @param {Boolean} [allowEqual=false]
   * @returns {FilterQuery}
   */
  greaterThan(attribute, value, allowEqual = false) {
    return this.where({
      range: {
        [attribute]: {
          [allowEqual ? "gte" : "gt"]: value
        }
      }
    });
  }

  /**
   * This method is used to add range (less than) condition to dsl
   *
   * @param {String} attribute
   * @param {String|Boolean|Number} value
   * @param {Boolean} [allowEqual=false]
   * @returns {FilterQuery}
   */
  lessThan(attribute, value, allowEqual = false) {
    return this.where({
      range: {
        [attribute]: {
          [allowEqual ? "lte" : "lt"]: value
        }
      }
    });
  }

  /**
   * This method is used to add has nested path condition to dsl
   *
   * @param {String} nestedPath
   * @param {Function} queryFunction
   * @returns {FilterQuery}
   */
  nested(nestedPath, queryFunction) {
    const JoinQueryBuilder = require("./Join");
    let nestedQuery = new JoinQueryBuilder();
    if (typeof queryFunction === 'function') {
      queryFunction(nestedQuery);
    }
    let nestedDSL = nestedQuery.getDSL();
    return this.where({
      nested: {
        path: nestedPath,
        query: nestedDSL.query,
        inner_hits: nestedDSL.inner_hits
      }
    });
  }

  /**
   * This method is used to add has child condition to dsl
   *
   * @param {String} doc
   * @param {Function} queryFunction
   * @returns {FilterQuery}
   */
  hasChild(doc, queryFunction) {
    const JoinQueryBuilder = require("./Join");
    let childQuery = new JoinQueryBuilder();
    if (typeof queryFunction === 'function') {
      queryFunction(childQuery);
    }
    let childDSL = childQuery.getDSL();
    return this.where({
      has_child: {
        type: doc,
        query: childDSL.query,
        inner_hits: childDSL.inner_hits
      }
    });
  }

  /**
   * This method is used to add has parent condition to dsl
   *
   * @param {String} doc
   * @param {Function} queryFunction
   * @returns {FilterQuery}
   */
  hasParent(doc, queryFunction) {
    const JoinQueryBuilder = require("./Join");
    let parentQuery = new JoinQueryBuilder();
    if (typeof queryFunction === 'function') {
      queryFunction(parentQuery); 
    }
    let parentDSL = parentQuery.getDSL();
    return this.where({
      has_parent: {
        type: doc,
        query: parentDSL.query,
        inner_hits: parentDSL.inner_hits
      }
    });
  }
}

module.exports = FilterQuery;
