"use strict";
const Client = require("../src/EsClient");
const QueryBuilder = require("../src/QueryBuilder");
const { hooks } = use("@adonisjs/ignitor");
const { ServiceProvider } = use("@adonisjs/fold");

class EsClientProvider extends ServiceProvider {
  register() {
    this.app.singleton("EsClient", app => {
      return new Client(app.use("Adonis/Src/Config"));
    });
    this.app.bind("EsQueryBuilder", app => {
      return QueryBuilder;
    });
    hooks.before.httpServer(() => {
      const EsClient = use("EsClient");
      EsClient.monitorConnections();
    });
  }
}

module.exports = EsClientProvider;
